﻿

using System.Data.Entity;
using TermoTest.DataLayerModels;

namespace TermoTest.EntityFramework
{
    public class MyDatabaseContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        //public DbSet<Product> Products { get; set; }
        //public DbSet<Dish> Dishes { get; set; }
        //public DbSet<DishProduct> DishProducts { get; set; }
        //public DbSet<DishOrder> DishOrders { get; set; }
        //public DbSet<Order> Orders { get; set; }
        //public DbSet<Customer> Customers { get; set; }

        public MyDatabaseContext() : base("MyDataBase")
        {

        }

    }
}
