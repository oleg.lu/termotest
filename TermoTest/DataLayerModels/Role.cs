﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TermoTest.DataLayerModels
{
    [Table("Roles", Schema = "public")]
    public class Role : BaseEntity
    {
        [Column("Name")]
        public string Name { get; set; }
        [Column("IsAdmin")]
        public bool IsAdmin { get; set; }

    }
}
