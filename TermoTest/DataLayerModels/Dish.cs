﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TermoTest.DataLayerModels
{
    [Table("Dishes", Schema = "public")]
    public class Dish : BaseEntity
    {
        [Column("Name")]
        public string? Name { get; set; }
        [Column("Price")]
        public int Price { get; set; }
        //public IList<DishOrder>? DishOrder { get; set; }
        //public IList<DishProduct>? DishProduct { get; set; }
    }
}
