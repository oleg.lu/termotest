﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TermoTest.DataLayerModels
{
    [Table("Products", Schema = "public")]
    public class Product : BaseEntity
    {
        [Column("Name")]
        public string? Name { get; set; }
        [Column("Weight")]
        public float Weight { get; set; }
        [Column("NumberInStock")]
        public int NumberInStock { get; set; }
        //[InverseProperty("DishProducts")]
        //public IList<DishProduct>? DishProduct { get; set; }
    }
}
