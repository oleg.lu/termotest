﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TermoTest.DataLayerModels
{
    [Table("Users", Schema = "public")]
    public class User : BaseEntity
    {
        [Column("Fullname")]
        public string Fullname { get; set; }
        [Column("Email")]
        public string Email { get; set; }
        [Column("Loggin")]
        public string Loggin { get; set; }
        [Column("Password")]
        public string Password { get; set; }
        [Column("RoleId")]
        public int RoleId { get; set; }
    }
}
