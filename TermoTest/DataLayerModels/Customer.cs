﻿using System;
using System.Collections.Generic;

namespace TermoTest.DataLayerModels
{
    public class Customer : BaseEntity
    {
        public string Name { get; set; }
        public IEnumerable<Order> Orders { get; set; }
        public DateTime RegistrationDate { get; set; }

    }
}
