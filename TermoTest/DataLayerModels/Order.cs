﻿using System;
using System.Collections.Generic;

namespace TermoTest.DataLayerModels
{
    public class Order : BaseEntity
    {
        public string? Name { get; set; }
        public int CurrentCustomerId { get; set; }
        public Customer? Customer { get; set; }
        public IList<DishOrder>? DishOrder { get; set; }
        public int OrderStatus { get; set; }
        public double Price { get; set; }
        public DateTime SaleDate { get; set; }
    }
}
