﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TermoTest.DataLayerModels
{
    [Table("DishProducts", Schema = "public")]
    public class DishProduct
    {
        [Column("DishId")]
        public int DishId { get; set; }
        [ForeignKey("DishId")]
        public Dish? Dish { get; set; }
        [Column("ProductId")]
        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public Product? Product { get; set; }

    }
}
