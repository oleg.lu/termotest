﻿using System;
using System.Linq;
using TermoTest.DataLayerModels;
using TermoTest.EntityFramework;

namespace TermoTest
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // получение данных
            using (MyDatabaseContext db = new MyDatabaseContext())
            {
                //var users = db.Users.ToList();
                //var roles = db.Roles.ToList();

                //var a = from u in db.Users
                //    where u.RoleId>1
                //    group u by u.RoleId ;
                //var resa = a.ToList();

                var b = (from u in db.Users
                         join r in db.Roles on u.RoleId equals r.Id
                         group r by r.IsAdmin into grp

                         //select grp
                         from g in grp
                         where g.Name == "Assistant"
                         select g
                         ).ToList();
            }
        }
    }
}
